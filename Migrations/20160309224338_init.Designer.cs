using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using BaneApp.Models;

namespace BaneApp.Migrations
{
    [DbContext(typeof(BaneAppContext))]
    [Migration("20160309224338_init")]
    partial class init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("BaneApp.Models.Line", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LineColor");

                    b.Property<int>("LineId");

                    b.Property<string>("Name");

                    b.Property<int>("Transportation");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("BaneApp.Models.Stop", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<string>("ShortName");

                    b.Property<int>("StopId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("BaneApp.Models.StopByLine", b =>
                {
                    b.Property<int>("StopByLineId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("LineId");

                    b.Property<int>("Order");

                    b.Property<int>("StopId");

                    b.HasKey("StopByLineId");
                });
        }
    }
}
