using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Newtonsoft.Json;
using System.Data.SqlClient;
using BaneApp.Models;

namespace BaneApp.Controllers
{
    using System.Net;
    using System.Text;

    public class HomeController : Controller
    {
        private BaneAppContext _context;
        
        public HomeController(BaneAppContext context)
        {
            _context = context;    
        }
        
        public IActionResult Index()
        {
            return View(_context.Stops.ToList());
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            var urlGetLines = "http://reisapi.ruter.no/Line/GetLines?json=true";
            var rLines = _download_serialized_json_data<List<RuterLine>>(urlGetLines);

            // using (var db = _context )
            // {
            //     db.Lines.
            // }
            // _context.Database.ExecuteSqlCommand("");
            // _context.Database.ExecuteSqlCommand("TRUNCATE TABLE [TableName]");
            // _context.Database.ExecuteStoreCommand("");
            // var conString = @"Server=.\SQLEXPRESS;Database=BaneAppDb;Trusted_Connection=True;";
            // using (SqlConnection connection = new SqlConnection(conString))
	        // {
            //     string sqlTrunc = "TRUNCATE TABLE Line";
            //     SqlCommand cmd = new SqlCommand(sqlTrunc, connection);
            //     cmd.ExecuteNonQuery();
            // }
            // Finne alle tbane linjene
            foreach (var r in rLines)
            {
                // Transportation 8 == Metro
                if (r.Transportation == 8)
                { 
                    var l = new Line();
                    l.LineId = r.ID;
                    l.Name = r.Name;
                    l.Transportation = r.Transportation;
                    l.LineColor = r.LineColour;

                    _context.Lines.Add(l);
                    _context.SaveChanges();
                    
                    // Hente ut alle stoppestedene for linjen vi akkurat har funnet
                    var urlGetStopsByLine = "http://reisapi.ruter.no/Line/GetStopsByLineId/" + r.ID + "?json=true";
                    var rStopsByLine = _download_serialized_json_data<List<RuterStopsByLine>>(urlGetStopsByLine);
                    
                    int order = 0;
                    foreach (var sl in rStopsByLine)
                    {
                        var s = new StopByLine();
                        order++;
                        s.LineId = r.ID;
                        s.StopId = sl.ID;
                        s.Order = order;
                
                        _context.StopsByLine.Add(s);
                        _context.SaveChanges();

                        // Finne data for et stoppested
                        var urlGetStop = "http://reisapi.ruter.no/place/getstop/" + sl.ID + "?json=true";
                        var rStop = _download_serialized_json_data<RuterStop>(urlGetStop);

                        var stop = new Stop();
                        stop.StopId = rStop.ID;
                        stop.Name = rStop.Name;
                        stop.ShortName = rStop.ShortName;
                        
                        // Sjekke om raden finnes allerede i basen
                        var finnes = _context.Stops.FirstOrDefault(x => x.StopId == stop.StopId);
                        if ( finnes == null ) 
                        {
                            _context.Stops.Add(stop);
                            _context.SaveChanges();
                        }
                    }
                }
            }
      
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        private static T _download_serialized_json_data<T>(string url) where T : new()
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    w.Encoding = Encoding.UTF8;
                    json_data = w.DownloadString(url);
                }
                catch (Exception) { }
                // if string with JSON data is not empty, deserialize it to class and return its instance 
                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
            }
        }
    }
}
