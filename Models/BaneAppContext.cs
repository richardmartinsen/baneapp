using Microsoft.Data.Entity;

namespace BaneApp.Models
{
    public class BaneAppContext : DbContext
    {
        public DbSet<Stop> Stops { get; set; }
        public DbSet<StopByLine> StopsByLine { get; set; }
        public DbSet<Line> Lines { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Stop>()
        //        .Property(s => s.StopId)
        //        .IsRequired();    
        //}
    }
}